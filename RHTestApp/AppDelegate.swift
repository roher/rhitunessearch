//
//  AppDelegate.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navigationController: UINavigationController!
    
    private var apiManager = ApiManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        setupWindow()
        
        return true
    }
    
    private func setupWindow() {
        let mainViewController = prepareMainViewController()
        navigationController = UINavigationController(rootViewController: mainViewController)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
    
    private func prepareMainViewController() -> UIViewController {
        
        // TODO: Create FlowCoordinator
        
        // Managing logic around Controller is based on MVP pattern : https://medium.com/ios-os-x-development/ios-architecture-patterns-ecba4c38de52#.jdqln7nca
        // In that way ViewController is very slim and all parts of this architecture are easy to UnitTest 😉
        // Additionally I added 'QuerySearchWorker' which controll search api operations.
        let searchApiManager = apiManager.getQuerySearchManager()
        let querySearchWorker = QuerySearchWorker(querySearchManager: searchApiManager)
        let mainViewController = MainViewController()
        let presenter = MainViewPresenter(viewController: mainViewController, queryWorker: querySearchWorker)
        mainViewController.presenter = presenter
        mainViewController.title = " 🎼iTunes search 🎵"
        
        return mainViewController
    }
}

