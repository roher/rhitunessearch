//
//  QuerySearchWorker.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import Foundation

protocol QuerySearchWorkerProtocol {
    func performSearch(with queryString: String,
                       completion: @escaping SearchQueryCompletion)
}

class QuerySearchWorker: QuerySearchWorkerProtocol {
    
    private lazy var operationQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        queue.qualityOfService = .userInitiated
        
        return queue
    }()
    
    private var latestOperation: QuerySearchUnitOperation?
    private let querySearchManager: QuerySearchManagerProtocol
    
    init(querySearchManager: QuerySearchManagerProtocol) {
        self.querySearchManager = querySearchManager
    }
    
    
    // Only the latest request is important for us. So we need to cancel and ignore response from previous one.
    func performSearch(with queryString: String,
                       completion: @escaping SearchQueryCompletion) {
        // Cancel actual operation
        latestOperation?.cancel()
        
        // Create new operation for search
        latestOperation = QuerySearchUnitOperation(queryString: queryString,
                                                   querySearchManager: querySearchManager,
                                                   completion: completion)
        
        operationQueue.addOperation(latestOperation!)
    }
    
    deinit {
        latestOperation?.cancel()
        latestOperation = nil
        
        operationQueue.cancelAllOperations()
    }
}
