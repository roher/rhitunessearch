//
//  QuerySearchUnitOperation.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import Foundation

class QuerySearchUnitOperation: RHOperation {
    
    private var ignoreResponse = false
    
    private var querySearchOperation: OperationProxyProtocol?
    private let queryString: String
    private let querySearchManager: QuerySearchManagerProtocol
    private let completion: SearchQueryCompletion
    
    init(queryString: String,
         querySearchManager: QuerySearchManagerProtocol,
         completion: @escaping SearchQueryCompletion) {
        self.queryString = queryString
        self.querySearchManager = querySearchManager
        self.completion = completion
    }
    
    override func execute() {
        if isCancelled || ignoreResponse {
            finish()
            
            return
        }
        
        querySearchOperation = querySearchManager.performSearch(with: queryString) { [weak self] success, response in
            guard let strongSelf = self else { return }
            if strongSelf.isCancelled || strongSelf.ignoreResponse {
                strongSelf.finish()
                
                return
            }
            
            strongSelf.markAsFinish(success: success, response: response)
        }
    }
    
    override func cancel() {
        super.cancel()
        
        querySearchOperation?.cancel()
        ignoreResponse = true
    }
    
    private func markAsFinish(success: Bool, response: QuerySearchResponse?) {
        DispatchQueue.main.async {
            self.completion(success, response)
        }
        
        finish()
    }
}
