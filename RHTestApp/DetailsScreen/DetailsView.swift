//
//  DetailsView.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import UIKit

class DetailsView: UIView {
    
    private let trackNameLabel = UILabel(frame: .zero)
    private let albumLabel = UILabel(frame: .zero)
    private let artistNameLabel = UILabel(frame: .zero)
    private let artworkImageView = UIImageView(frame: .zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // In this place the best way will be to use StackView
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let topMargin = CGFloat(15)
        
        // Artwork
        let artworkSideheightRatio = CGFloat(0.75)
        let artworkSideHeight = bounds.width * artworkSideheightRatio
        artworkImageView.frame = CGRect(x: bounds.width/2 - artworkSideHeight/2,
                                        y: topMargin,
                                        width: artworkSideHeight,
                                        height: artworkSideHeight)
        artworkImageView.layer.cornerRadius = artworkSideHeight/2
        
        
        let horizontalMargin = CGFloat(15)
        
        // Track
        trackNameLabel.sizeToFit()
        trackNameLabel.frame = CGRect(x: horizontalMargin,
                                      y: artworkImageView.frame.maxY + topMargin,
                                      width: bounds.width - 2*horizontalMargin,
                                      height: trackNameLabel.bounds.height)
        
        // Album
        albumLabel.sizeToFit()
        albumLabel.frame = CGRect(x: horizontalMargin,
                                  y: trackNameLabel.frame.maxY + topMargin,
                                  width: bounds.width - 2*horizontalMargin,
                                  height: albumLabel.bounds.height)
        
        // Artist
        artistNameLabel.sizeToFit()
        artistNameLabel.frame = CGRect(x: horizontalMargin,
                                       y: albumLabel.frame.maxY + topMargin,
                                       width: bounds.width - 2*horizontalMargin,
                                       height: artistNameLabel.bounds.height)
    }
    
    func setArtworkView(image: UIImage) {
        artworkImageView.image = image
    }
    
    func setTrackLabel(with title: String) {
        trackNameLabel.text = title
        setNeedsLayout()
    }
    
    func setAlbumLabel(with title: String) {
        albumLabel.text = title
        setNeedsLayout()
    }
    
    func setArtistLabel(with name: String) {
        artistNameLabel.text = name
        setNeedsLayout()
    }
    
    private func setup() {
        backgroundColor = .white
        
        trackNameLabel.textAlignment = .center
        addSubview(trackNameLabel)
        
        albumLabel.textAlignment = .center
        addSubview(albumLabel)
        
        artistNameLabel.textAlignment = .center
        addSubview(artistNameLabel)
        
        artworkImageView.clipsToBounds = true
        addSubview(artworkImageView)
    }
}
