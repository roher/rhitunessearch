//
//  DetailsViewPresenter.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import Foundation

protocol DetailsViewPresenterProtocol {
    func getSearchItem() -> SearchItem
}

class DetailsViewPresenter: DetailsViewPresenterProtocol {
    
    private let searchItem: SearchItem
    
    unowned var viewController: DetailsViewPresenterConnectionProtocol
    
    init(viewController: DetailsViewPresenterConnectionProtocol, searchItem: SearchItem) {
        self.viewController = viewController
        self.searchItem = searchItem
    }
    
    func getSearchItem() -> SearchItem {
        return searchItem
    }
}
