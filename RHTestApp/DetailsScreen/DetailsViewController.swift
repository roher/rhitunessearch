//
//  DetailsViewController.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import UIKit

protocol DetailsViewPresenterConnectionProtocol: class { /* Empty implementation */ }

class DetailsViewController: UIViewController, DetailsViewPresenterConnectionProtocol {
    
    private let detailsView = DetailsView()
    var presenter: DetailsViewPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    override func loadView() {
        view = detailsView
    }
    
    func configureView() {
        let searchItem = presenter.getSearchItem()
        
        detailsView.setAlbumLabel(with: searchItem.albumName)
        detailsView.setTrackLabel(with: searchItem.trackName)
        detailsView.setArtistLabel(with: searchItem.artistName)
        detailsView.setArtworkView(image: #imageLiteral(resourceName: "placeholder"))
    }
    
    private func setup() {
        edgesForExtendedLayout = []
        
        configureView()
    }
}
