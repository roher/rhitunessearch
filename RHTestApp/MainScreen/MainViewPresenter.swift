//
//  MainViewPresenter.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import Foundation

protocol MainViewPresenterProtocol {
    func numberOfItems() -> Int
    func item(at index: Int) -> SearchItem?
    func performSearch(with queryString: String)
}

class MainViewPresenter: MainViewPresenterProtocol {
    
    private let querySearchWorker: QuerySearchWorkerProtocol
    fileprivate var searchResults: QuerySearchResponse? = nil
    
    unowned var viewController: MainViewPresenterConnectionProtocol
    
    init(viewController: MainViewPresenterConnectionProtocol, queryWorker: QuerySearchWorkerProtocol) {
        self.viewController = viewController
        self.querySearchWorker = queryWorker
    }
    
    func numberOfItems() -> Int {
        return searchResults?.allItems.count ?? 0
    }
    
    func item(at index: Int) -> SearchItem? {
        let item = searchResults?.allItems[index]
        
        return item
    }
    
    func performSearch(with queryString: String) {
        querySearchWorker.performSearch(with: queryString) {  [weak self] success, response in
            guard let strongSelf = self else { return }
            
            strongSelf.searchResults = response
            strongSelf.viewController.performViewUpdate()
        }
    }
}
