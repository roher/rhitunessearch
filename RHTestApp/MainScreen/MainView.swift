//
//  MainView.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import UIKit

class MainView: UIView {
    
    let searchBar = UISearchBar(frame: .zero)
    let tableView = UITableView(frame: .zero, style: .plain)
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // SearchBar
        let searchBarHeight = CGFloat(50)
        searchBar.frame = CGRect(x: 0,
                                 y: 0,
                                 width: bounds.width,
                                 height: searchBarHeight)
        
        // TableView
        let tableViewHeight = bounds.height - searchBar.frame.maxY
        tableView.frame = CGRect(x: 0,
                                 y: searchBar.frame.maxY,
                                 width: bounds.width,
                                 height: tableViewHeight)
    }
    
    func setup() {
        backgroundColor = .white
        
        addSubview(searchBar)
        addSubview(tableView)
    }
}
