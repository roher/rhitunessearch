//
//  ViewController.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import UIKit

protocol MainViewPresenterConnectionProtocol: class {
    func performViewUpdate()
}

class MainViewController: UIViewController {
    
    fileprivate let cellIdentifier = "MainView_cell_id"
    fileprivate var mainView = MainView()
    var presenter: MainViewPresenterProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    override func loadView() {
        view = mainView
    }
    
    func setup() {
        edgesForExtendedLayout = []
        
        setupSearchBar()
        setupTableView()
    }
    
    func setupSearchBar() {
        mainView.searchBar.delegate = self
    }
    
    func setupTableView() {
        let tableView = mainView.tableView
    
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    fileprivate func pushDetailsViewController(with item: SearchItem) {
        let detailsViewController = DetailsViewController()
        let presenter = DetailsViewPresenter(viewController: detailsViewController, searchItem: item)
        detailsViewController.presenter = presenter
        
        navigationController?.pushViewController(detailsViewController, animated: true)
    }
}

extension MainViewController: MainViewPresenterConnectionProtocol {
    
    func performViewUpdate() {
        mainView.tableView.reloadData()
    }
}

extension MainViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.performSearch(with: searchText)
    }
}

extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let searchItem = presenter.item(at: indexPath.row) else { return }
        
        // Of course all routing should be moved from viewcontroller to some FlowCoordinator mechanism. e.g.: https://vimeo.com/144116310
        // It is not the good practice to keep routing inside viewcontrollers
        pushDetailsViewController(with: searchItem)
    }
}

extension MainViewController:  UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // TODO: good thing will be to add Configurator cell class 😇
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if (cell == nil)  {
            cell = UITableViewCell(style: .subtitle,
                                   reuseIdentifier: cellIdentifier)
        }
    
        if let resultItem = presenter.item(at: indexPath.row) {
            cell!.accessoryType = .disclosureIndicator
            
            cell!.textLabel?.text = resultItem.trackName
            cell!.detailTextLabel?.text = resultItem.artistName
            
            // Sorry for that, but because I have a lot of own work I didn't had time to create
            // good mechanism to fetch images from the backend 😔 So you will see only placeholder
            cell!.imageView?.image = #imageLiteral(resourceName: "placeholder")
        }
        
        return cell!
    }
}
