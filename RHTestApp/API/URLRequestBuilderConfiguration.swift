//
//  URLRequestBuilderConfiguration.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import Foundation

protocol URLRequestBuilderConfigurationProtocol {
    var baseUrl: URL { get }
    var httpMethod: HTTPMethods { get }
    var queryItems: [URLQueryItem]? { get }
}

struct URLRequestBuilderConfiguration: URLRequestBuilderConfigurationProtocol {
    let baseUrl: URL
    let httpMethod: HTTPMethods
    private(set) var queryItems: [URLQueryItem]?
}
