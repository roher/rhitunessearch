//
//  ApiManager.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import Foundation

protocol ApiManagerProtocol: ApiManagerDelegateProtocol {
    func getQuerySearchManager() -> QuerySearchManagerProtocol
}

protocol ApiManagerDelegateProtocol: class  {
    func addToQueue(operation: Operation)
}

class ApiManager: ApiManagerProtocol {
    
    private lazy var operationQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.qualityOfService = .userInitiated
        
        return queue
    }()
    
    private let querySearchManager: QuerySearchManagerProtocol
    
    init(querySearchManager: QuerySearchManagerProtocol) {
        self.querySearchManager = querySearchManager
        self.querySearchManager.delegate = self
    }
    
    convenience init() {
        let querySearchManager = QuerySearchManager()
        
        self.init(querySearchManager: querySearchManager)
    }
    
    func addToQueue(operation: Operation) {
        operationQueue.addOperation(operation)
    }
    
    func getQuerySearchManager() -> QuerySearchManagerProtocol {
        return querySearchManager
    }
}
