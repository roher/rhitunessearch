//
//  QuerySearchOperation.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import Foundation

class QuerySearchOperation: RHOperation {
    
    private let session: URLSession
    private let requestBuilder: URLRequestBuilderProtocol
    private let parser: QuerySearchParserProtocol
    private let completion: SearchQueryCompletion
    private var task: URLSessionDataTask?
    
    init(session: URLSession,
         requestBuilder: URLRequestBuilderProtocol,
         parser: QuerySearchParserProtocol,
         completion: @escaping SearchQueryCompletion) {
        self.session = session
        self.requestBuilder = requestBuilder
        self.parser = parser
        self.completion = completion
    }
    
    override func execute() {
        let request = requestBuilder.build()
        
        task = session.dataTask(with: request) { [weak self] data, response, error in
            guard let strongSelf = self else { return }
            
            if error != nil {
                strongSelf.markAsFinish(success: false, response: nil)
                return
            }
            
            var response: QuerySearchResponse?
            if let data = data {
                // Parsing is processed on the background thread
                response = strongSelf.parser.parse(data: data)
            }
            
            let success = response != nil ? true : false
            strongSelf.markAsFinish(success: success, response: response)
        }
        
        task?.resume()
    }
    
    override func cancel() {
        super.cancel()
        
        task?.cancel()
    }
    
    private func markAsFinish(success: Bool, response: QuerySearchResponse?) {
        DispatchQueue.main.async {
            self.completion(success, response)
        }
        
        finish()
    }
}
