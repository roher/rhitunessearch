//
//  QuerySearchResponse.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

struct QuerySearchResponse {
    let allItems: [SearchItem]
}
