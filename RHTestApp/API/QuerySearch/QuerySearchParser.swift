//
//  QuerySearchParser.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import Foundation

protocol QuerySearchParserProtocol {
    func parse(data: Data) -> QuerySearchResponse?
}

class QuerySearchParser: QuerySearchParserProtocol {
    
    func parse(data: Data) -> QuerySearchResponse? {
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any],
                let itemsArray = json["results"] as? [[String: Any]] else { return nil }
            
            var artists = [SearchItem]()
            for item in itemsArray {
                let artist = SearchItem(with: item)
                artists.append(artist)
            }
            
            return QuerySearchResponse(allItems: artists)
        } catch {
            print("JSON data serialization problem")
        }
        
        return nil
    }
}
