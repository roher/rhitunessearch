//
//  QuerySearchOperationBuilder.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import Foundation

typealias SearchQueryCompletion = (_ success: Bool, _ response: QuerySearchResponse?)->()

protocol QuerySearchOperationBuilderProtocol {
    func performSearch(with session: URLSession,
                       queryString: String,
                       completion: @escaping SearchQueryCompletion) -> Operation
}

class QuerySearchOperationBuilder: QuerySearchOperationBuilderProtocol {
    
    func performSearch(with session: URLSession,
                       queryString: String,
                       completion: @escaping SearchQueryCompletion) -> Operation {
        let url = URL(string: ApiUrlStringSet.querySearch)
        if url == nil {
            assertionFailure("You are using incorrect URL string")
        }
        
        let queryItems = generateSearchQueryItems(with: queryString)
        let requestBuilderConfiguration = URLRequestBuilderConfiguration(baseUrl: url!,
                                                                         httpMethod: .GET,
                                                                         queryItems: queryItems)
        let requestBuilder = URLRequestBuilder(configuration: requestBuilderConfiguration)
        let parser = QuerySearchParser()
        let opreation = QuerySearchOperation(session: session,
                                             requestBuilder: requestBuilder,
                                             parser: parser,
                                             completion: completion)
        
        return opreation
    }
    
    private func generateSearchQueryItems(with queryString: String) -> [URLQueryItem] {
        let queryItem = URLQueryItem(name: "term", value: queryString)
        
        return [queryItem]
    }
}
