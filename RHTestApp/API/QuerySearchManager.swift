//
//  QuerySearchManager.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import Foundation

protocol QuerySearchManagerProtocol {
    weak var delegate: ApiManagerDelegateProtocol? { get set }
    func performSearch(with queryString: String,
                       completion: @escaping SearchQueryCompletion) -> OperationProxyProtocol
}

class QuerySearchManager: QuerySearchManagerProtocol {
    
    weak var delegate: ApiManagerDelegateProtocol?
    
    private lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration,
                                 delegate: nil,
                                 delegateQueue: nil)
        
        return session
    }()
    
    func performSearch(with queryString: String,
                       completion: @escaping SearchQueryCompletion) -> OperationProxyProtocol {
        
        let querySearchOperationBuilder = QuerySearchOperationBuilder()
        let operation = querySearchOperationBuilder.performSearch(with: session,
                                                                  queryString: queryString,
                                                                  completion: completion)
        
        delegate?.addToQueue(operation: operation)
        
        return OperationProxy(operation: operation)
    }
}
