//
//  OperationProxy.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import Foundation

protocol OperationProxyProtocol {
    init(operation: Operation)
    
    func cancel()
}

/**
 Class created for hide request operation outside of ApiManager class, because this operation should only be used in ApiManager operationQueue.
 OperationProxy class should be used to controll request operation outside of ApiManager.
 */
class OperationProxy: OperationProxyProtocol {
    
    private let operation: Operation
    
    required init(operation: Operation) {
        self.operation = operation
    }
    
    func cancel() {
        operation.cancel()
    }
}
