//
//  URLRequestBuilder.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import Foundation

protocol URLRequestBuilderProtocol {
    func build() -> URLRequest
}

struct URLRequestBuilder: URLRequestBuilderProtocol {
    
    let configuration: URLRequestBuilderConfigurationProtocol
    
    func build() -> URLRequest { // TODO: create configurator class
        let url = buildUrlForRequest()
        
        if url == nil {
            assertionFailure("You are using incorrect URL string")
        }
        
        var urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = configuration.httpMethod.rawValue
        
        return urlRequest
    }
    
    private func buildUrlForRequest() -> URL? {
        let baseUrl = configuration.baseUrl
        let urlComponents = NSURLComponents()
        urlComponents.scheme = baseUrl.scheme
        urlComponents.host = baseUrl.host
        urlComponents.path = baseUrl.path
        
        
        if let queryItems = configuration.queryItems {
            urlComponents.queryItems = queryItems
        }
        
        return urlComponents.url
    }
}
