//
//  SearchItem.swift
//  RHTestApp
//
//  Created by Robert Herdzik on 06/01/2017.
//  Copyright © 2017 Robert Herdzik. All rights reserved.
//

import Foundation

class SearchItem {
    
    let trackName: String
    let artistName: String
    let albumName: String
    let largeThumbnailUrlString: String
    
    init(trackName: String,
         artistName: String,
         albumName: String,
         largeThumbnailUrlString: String) {
        self.trackName = trackName
        self.artistName = artistName
        self.albumName = albumName
        self.largeThumbnailUrlString = largeThumbnailUrlString
    }
    
    init(with data: [String: Any]) {
        self.trackName = data["trackName"] as? String ?? ""
        self.artistName = data["artistName"] as? String ?? ""
        self.albumName = data["collectionName"] as? String ?? ""
        self.largeThumbnailUrlString = data["artworkUrl100"] as? String ?? ""
    }
}
