# SUMMARY #

At the beginning I would like to say sorry because I hadn't time to finish all task. I think that already implemented part will tell you something about my style.

I use mostly ```Operation``` and ```OperationsQueue``` for managing tasks. 
```QuerySearchWorker``` was created because we need to manage operations which we are no longer interested. I know that this class required some Interval mechanism using which we will be able to delay creating request between each typed in character. 

In my all projects for application routing purposes I use FlowCoordinator class, in this sample project I didn't had enought time to implement it (so application flow now is close related with ViewController 🙁). 

I use MVP patter for managing logic around controllers in that way view controllers are dummy,

Mechanism for fetching images wasn't implemented, it is not something complex for me but time consumming, as I sad I don't have enought time to fully complete your task, but I belive that my code will tell you enought about my style.

Also mechanism for managing table view bottom inset according keyboard height should be implemented.

## Thank you for cool excercise 😊
